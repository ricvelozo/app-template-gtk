option('channel',
  type: 'combo',
  choices: ['stable', 'beta', 'nightly'],
  description: 'The distribution channel',
)
