#include <adwaita.h>
#include <glib/gi18n.h>
#include <locale.h>
#include <stdio.h>

#include "config.h"
#include "main-window.h"

static void
on_startup (AdwApplication *app)
{
  g_assert (GTK_IS_APPLICATION (app));

  g_set_application_name (_("App Template"));
}

static void
on_activate (AdwApplication *app)
{
  GtkWindow *window;
  
  g_assert (GTK_IS_APPLICATION (app));
  
  window = gtk_application_get_active_window (GTK_APPLICATION (app));
  if (window == NULL)
    window = g_object_new (MAIN_TYPE_WINDOW,
		                       "application", app,
		                       NULL);
  
  gtk_window_present (window);
}

int
main (int   argc,
      char *argv[])
{
  g_autoptr(AdwApplication) app = NULL;

  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALE_DIR);
  textdomain (PACKAGE);
  
  app = adw_application_new (APP_ID, G_APPLICATION_FLAGS_NONE);
  g_signal_connect (app, "startup", G_CALLBACK (on_startup), NULL);
  g_signal_connect (app, "activate", G_CALLBACK (on_activate), NULL);

  return g_application_run (G_APPLICATION (app), argc, argv);
}
