#include "config.h"
#include "main-window.h"

struct _MainWindow
{
  AdwApplicationWindow  parent_instance;

  GtkHeaderBar          *left_headerbar;
  GtkHeaderBar          *right_headerbar;
  GtkMenuButton         *main_menu;
  GtkMenuButton         *recent_list;
};

G_DEFINE_TYPE (MainWindow, main_window, ADW_TYPE_APPLICATION_WINDOW)

static void
on_inspector_clicked (void)
{
  gtk_window_set_interactive_debugging (TRUE);
}

static void
main_window_class_init (MainWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/br/com/ricardoveloso/AppTemplate/main-window.ui");
  gtk_widget_class_bind_template_child (widget_class, MainWindow, left_headerbar);
  gtk_widget_class_bind_template_child (widget_class, MainWindow, right_headerbar);
  gtk_widget_class_bind_template_child (widget_class, MainWindow, main_menu);
  gtk_widget_class_bind_template_child (widget_class, MainWindow, recent_list);
  gtk_widget_class_bind_template_callback (widget_class, on_inspector_clicked);
}

static void
main_window_init (MainWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
